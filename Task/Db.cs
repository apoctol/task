﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestTask
{
    public class Sale
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public ICollection<Contractors> Contractors { get; set; }
      //  [ForeignKey("Contacts")]
        public int? ContactsId { get; set; }
        public Contacts Contacts { get; set; }

        public Sale()
        {
            Contractors = new List<Contractors>();
        }
    }
    public class Contractors
    {
        public int Id { get; set; }
        public string NameCompany { get; set; }
       // [ForeignKey("Sale")]
        public int? SaleId { get; set; }
        public virtual Sale Sale { get; set; }
      //  [ForeignKey("City")]
        public int? CityId { get; set; }
        public virtual City City { get; set; }
    }
    public class City
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public ICollection<Contractors> Contractor { get; set; }

        public City()
        {
            Contractor = new List<Contractors>();
        }
    }
    public class Contacts
    {
        public int ContactsId { get; set; }
        public string Name { get; set; }
        public string ContactIngo { get; set; }
       public  ICollection<Sale> Sale { get; set; }

        public Contacts()
        {
            Sale = new List<Sale>();
        }
    }

}