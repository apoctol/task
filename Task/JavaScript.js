﻿// For this demo, we are using the UMD build of react-router-dom
const {
    HashRouter,
    Switch,
    Route,
    Link } =
    ReactRouterDOM;

// A simple data API that will be used to get the data for our
// components. On a real website, a more robust data fetching
// solution would be more appropriate.
const PlayerAPI = {
    players: [
        { number: 1, name: "Ben Blocker", position: "G" },
        { number: 2, name: "Dave Defender", position: "D" },
        { number: 3, name: "Sam Sweeper", position: "D" },
        { number: 4, name: "Matt Midfielder", position: "M" },
        { number: 5, name: "William Winger", position: "M" },
        { number: 6, name: "Fillipe Forward", position: "F" }],

    all: function () { return this.players; },
    get: function (id) {
        const isPlayer = p => p.number === id;
        return this.players.find(isPlayer);
    }


    // The FullRoster iterates over all of the players and creates
    // a link to their profile page.
}; const FullRoster = () =>
    React.createElement("div", null,
        React.createElement("ul", null,

            PlayerAPI.all().map((p) =>
                React.createElement("li", { key: p.number },
                    React.createElement(Link, { to: `/roster/${p.number}` }, p.name)))));







// The Player looks up the player using the number parsed from
// the URL's pathname. If no player is found with the given
// number, then a "player not found" message is displayed.
const Player = props => {
    const player = PlayerAPI.get(
        parseInt(props.match.params.number, 10));

    if (!player) {
        return React.createElement("div", null, "Sorry, but the player was not found");
    }
    return (
        React.createElement("div", null,
            React.createElement("h1", null, player.name, " (#", player.number, ")"),
            React.createElement("h2", null, "Position: ", player.position),
            React.createElement(Link, { to: "/roster" }, "Back")));


};

// The Roster component matches one of two different routes
// depending on the full pathname
const Roster = () =>
    React.createElement(Switch, null,
        React.createElement(Route, { exact: true, path: "/roster", component: FullRoster }),
        React.createElement(Route, { path: "/roster/:number", component: Player }));



const Schedule = () =>
    React.createElement("div", null,
        React.createElement("ul", null,
            React.createElement("li", null, "6/5 @ Evergreens"),
            React.createElement("li", null, "6/8 vs Kickers"),
            React.createElement("li", null, "6/14 @ United")));




const Home = () =>
    React.createElement("div", null,
        React.createElement("h1", null, "Welcome to the Tornadoes Website!"));



// The Main component renders one of the three provided
// Routes (provided that one matches). Both the /roster
// and /schedule routes will match any pathname that starts
// with /roster or /schedule. The / route will only match
// when the pathname is exactly the string "/"
const Main = () =>
    React.createElement("main", null,
        React.createElement(Switch, null,
            React.createElement(Route, { exact: true, path: "/", component: Home }),
            React.createElement(Route, { path: "/roster", component: Roster }),
            React.createElement(Route, { path: "/schedule", component: Schedule })));




// The Header creates links that can be used to navigate
// between routes.
const Header = () =>
    React.createElement("header", null,
        React.createElement("nav", null,
            React.createElement("ul", null,
                React.createElement("li", null, React.createElement(Link, { to: "/" }, "Home")),
                React.createElement("li", null, React.createElement(Link, { to: "/roster" }, "Roster")),
                React.createElement("li", null, React.createElement(Link, { to: "/schedule" }, "Schedule")))));





const App = () =>
    React.createElement("div", null,
        React.createElement(Header, null),
        React.createElement(Main, null));



// This demo uses a HashRouter instead of BrowserRouter
// because there is no server to match URLs
ReactDOM.render(
    React.createElement(HashRouter, null,
        React.createElement(App, null)),

    document.getElementById('root'));